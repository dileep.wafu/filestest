/* eslint-disable prettier/prettier */
const mongoose = require("mongoose");

const { MERAKIBROS_DB, DB_URI, DB_USERNAME, DB_PASSWORD, DB_AUTH_SOURCE } = require('../config');

module.exports = {
    getConnectionStringForDB: function (dbName) {
        if (dbName) {
            return "mongodb://" + DB_USERNAME + ":" + DB_PASSWORD + "@" + DB_URI + "/" + dbName + "?authSource=" + DB_AUTH_SOURCE;
            // return "mongodb://localhost/" + dbName;
        }
        throw new Error("DB Name is Empty");
    },

    getConnectionForDB: async function (dbName) {
        if (await this.checkDatabaseExistence(dbName)) {
            var dbConnector = "mongodb://" + DB_USERNAME + ":" + DB_PASSWORD + "@" + DB_URI + "/" + dbName + "?authSource=" + DB_AUTH_SOURCE;
            //var dbConnector = "mongodb://localhost/" + dbName;
            return await mongoose.connect(
                dbConnector,
                { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }
            );
        }
        throw new Error("DB doesn't exist");
    },

    getInitialConnectionForDB: async function () {
        const dbConnector = "mongodb://" + DB_USERNAME + ":" + DB_PASSWORD + "@" + DB_URI + "/" + MERAKIBROS_DB + "?authSource=" + DB_AUTH_SOURCE;
        return await mongoose.connect(
            dbConnector,
            { useNewUrlParser: true,  useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }
        );
    },

    checkDatabaseExistence: async function (dbName) {
        const Admin = mongoose.mongo.Admin;
        const databases = await new Admin(mongoose.connection.db).listDatabases();
        const jsonifiedDatabases = JSON.parse(JSON.stringify(databases.databases));
        for (let i = 0; i < jsonifiedDatabases.length; i+=1)
            if (jsonifiedDatabases[i].name === dbName)
                return true;
        return false;
    }
}